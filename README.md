# Service-util

This project contains several utility classes which simplify using of factories with annotations. For this it uses Javas service loader mechanism. 
Via annotations entries in files in the folder META-INF/services are generated for the annotated classes. This enables the user to use a simple plugin mechanism
because no explicit registration for the classes in the factory is needed. All classes of the type the factory is responsible for are registered automatically as
soon as they are listed in the appropriate service file.


The utilities are

- generic annotation processor for creating service entries in the META-INF file (like Google´s auto-service) but more specific
- generich factory class which loads the classes to be generated via the services file.
- a factory which generates the appropriate factory class for a type
- utility classes for internationalization

The following example shows how to use the classes in this project. In this example we want to create command classes. Each class is responsible 
for executing a specific task which is identified by a name. A factory class is used to create an instance of the class which is responsible for
the task.

## Define the annotation
Each command class must be annotated with an annotation. This annotation is used for
- defining the task the command is responsible for
- creating an entry in a file in the folder META-INF/services

Below you find an example of the annotations definition. In this example we call the annotation CommandHandler. The attribute "type" defines the task the command is 
responsible for and is used by the factory class to determine the correct instance. You can add further attributes to this annotation if needed.


```java
@Retention( RUNTIME )
@Target( { TYPE, METHOD } )
public @interface CommandHandler
{
    String type();

}
```

## Define the command interface
All command classes must inherit from a common interface. The name of the interface class (incl. package) is used as the name of the file in the folder META-INF/services. E.g.
if the interface is de.psc.command.Command the file name is also "de.psc.command.Command". In this file you find an entry with the command´s class name. The example uses
a very simple interface.

```java
public interface Command
{
    void process( String[] param );

}

```

## Define the annotation processor
The annotation processor is responsible for creating the entries in the service file. The base of the annotation processor class is an abstract generic class called 
"GenericServiceAnnotationProcessor" which provides all needed functionality. The simplest definition of your annotation processor class is.

```java
@AnnotationProcessor
public class CommandHandlerAnnotationProcessor extends GenericServiceAnnotationProcessor<CommandHandler>
{

    public CommandHandlerAnnotationProcessor(  )
    {
        super( CommandHandler.class );
        
    }
    
    @Override
    protected Option<DeclaredType> getServiceFieldOfClass( final AnnotationMirror annotationMirror )
    {
        
        Elements elements = processingEnv.getElementUtils();
        Types types = processingEnv.getTypeUtils();

        TypeElement processorElement = elements.getTypeElement( Command.class.getName());

        DeclaredType processorType = types.getDeclaredType( processorElement );
        
        return Option.of(  processorType );

    }

}
```

The annotation "AnnotationProessor" creates an entry for this class in the file "META-INF/services/javax.annotation.processor.Processor" so that the Processor is 
recognized as an AnnotationProcessor during compilation time. Please note that the definition of the annotation processor must be in a different jar file than
the definition of the Command implementations.
The function getServiceFieldOfClass returns an instance of DeclaredType which defines the interface for which a service entry must be created (In our case Command.class).
You do not have to override this function if the annotation has an attribute called "service" of type "Class<?>" which defines the interface (like Googles AutoService).


## Create the factory
The definition of the factory class for your commands is as follows:


```java
@Factory
public class CommandFactory extends AbstractServiceLoaderBasedFactory<Command, CommandHandler>
{
    
    public CommandFactory()
    {
        super( CommandHandler.class, Command.class );
        
    }

    @Override
    protected List<String> getKeys( final CommandHandler annotation )
    {
        return Option.of( annotation.type() );
        
    }

}
```
The "Factory" annotation is used to create an entry in the file "META-INF/services/de.psc.util.service.factory.GenericFactory" and is used for registrating your factory
in the GenericFactoryFactory class. You need to overwrite the standard constructor and the function getKeys as shown above. The example uses the attribute "type" of the 
annotation in order to determine the key the implementation is assigned to. You can use any attribute name. It is possible to assogn an implementation to more than one key.
In that case the used attribute of your annotation must be defined as as array and the list returned by the function getKeys contains all elements of the array.

Additionally you can overwrite the function "getContext" of AbstractServiceLoaderBasedFactory. The context can be used if you have different implementations for the same
type and the implementation to use depends on the context. E.g. a command might work differently depending on the type of your application (UI, Shell).

## Implement your classes
Now you can implement your classes. As mentioned above the implementations must be in a different jar file than the definition of the annotation processor. 


```java
@Command( type="DO_SOMETHING" )
public class DoSomethingCommand implements Command
{

    @Override
    public void process( final String[] arguments ) 
    {
        ... write your code here ...

    }

}
```

## Retrieve your implementation
When you want to get the implementation of your class you first need to get the factory for your command implementations. After having got the factory you
can ask for the implementation.

```java
Command command = GenericFactoryFactory.getFactoryFor( Command.class )
                                       .flatMap( ( t ) -> t.getImplementationFor( "DO_SOMETHING" ) )
                                       .get( );

```

If there exist more than one implementation for a key the function getImplementationFor returns the first entry in the list. There is no defined order. 
If you need to get all implementations for a key you can use the function getImplementationsFor.

## Internationalization
This project provides some classes for using internationalization.

### Messages
The utility class Messages simplifies the usage of ressource bundles. You do not have to create the resource bundles programmatically. Instead you define the resource bundle
to be used for a message key via configuration. The class Messages uses the first part of a key to determine the resource bundle. Therefore it uses the configuration mechanisms of
Apache Tamaya. I.e. you create a file called "javaconfiguration.properties" in the folder META-INF. This file must contain a key "resources.bundle.<prefix>" which defines
the name of the resource bundle. E.g. if your messages have the format "command.something.somewhat" and the resource bundle to be used is "shellcommand" the entry could be

```java
resources.bundle.command = shellcommand
```

The resource bundle shellcommand.properties (or the language specific files) must contain the message key and assign it to the text

```java
command.something.somewhat = Something happened with somewhat
```

If no resource bundle is defined for a key the class tries to use a default resource bundle which must be set explicitely by

```java
Messages.addBundle( Messages.DEFAULT_BUNDLE, yourBundle );
```

The configuration mechanism of Apache Tamaya allows you to overwrite the configuration of a dependency if the dependency also uses this kind of configuration for
resource bundles. See the documentation of Apache Tamaya for further explanations.

### InternationalizedException
This class is a small helper class for using exceptions with localized messages. Instead of passing a concrete message to subclasses of this class you pass a message key and
additional information. The utility class Messages is used to determine the correct message to use.


