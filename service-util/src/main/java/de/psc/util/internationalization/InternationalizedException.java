package de.psc.util.internationalization;

import lombok.Getter;
import lombok.NonNull;

/**
 * Abstract base class for Exceptions.
 * Exceptions which inherit from this class use resource bundles for defining the message to use.
 * When creating an instance of an exception at least the key and the name of the resource bundle must be passed.
 * The bundle name must be equal to the name which is used for registering the resouce bundle in the Messages class.
 *
 */
@Getter
public abstract class InternationalizedException extends RuntimeException
{
    /**
     * Serialization ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * The message key which is used for identifying the message
     */
    private String msgKey;
    
    /**
     * The values which are used to fill the placeholders in the message
     */
    private Object[] values;
    
    /**
     * Constructor
     * 
     * @param msgKey Key which is used to identify the message of the exception.
     * @param values Arrays of values which fill the placeholders in the message.
     */
    public InternationalizedException( final @NonNull String msgKey, final @NonNull Object[] values )
    {
        this.msgKey = msgKey;
        this.values = values;
        
    }
    /**
     * Constructor
     * 
     * @param msgKey Key which is used to identify the message of the exception.
     * 
     */
    public InternationalizedException( final @NonNull String msgKey )
    {
        this.msgKey = msgKey;
        this.values = new Object[] {};
        
    }
    
    
    /** 
     * Returns the formatted message of the key. If no message could be found the key is returned.
     * @return The formatted message identified by the key and bundle
     */
    @Override
    public String getMessage()
    {
        return Messages.getMessageFor( msgKey, values );
        
    }

}
