package de.psc.util.internationalization;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.tamaya.Configuration;
import org.apache.tamaya.ConfigurationProvider;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.NonNull;

/**
 * Central utility class for managing messages from different resource bundles.
 * You can register several resource bundles in this class. When requesting a message for a key the name of the bundle must be passed.
 * The bundle name defines the resource bundle where the message for a key must be searched in.
 *
 */
public class Messages
{
    public final static String DEFAULT_BUNDLE = "DEFAULT";
    
    /**
     * Map with the registered resource bundle
     */
    private static Map<String, ResourceBundle> registry = HashMap.empty();
    
    /**
     * The configuration of the project
     */
    private static Configuration config;
    
    static 
    {
        config = ConfigurationProvider.getConfiguration();
        
    }
    
    
    /**
     * Adds a new bundle to the registry. If there is already a bundle with this name registered, the already registered bundle
     * will be overwritten by the new one. The used name is the base name of the bundle.
     * @param key the key under which this bundle is stored in the registry.
     * @param bundle The bundle to be registered.
     */
    public static void addBundle ( final @NonNull String key, final @NonNull ResourceBundle bundle )
    {
        registry = registry.put( key, bundle );
        
    }
    
    /**
     * Adds a new bundle to the registry. If there is already a bundle with this name registered, the already registered bundle
     * will be overwritten by the new one. The used name is the base name of the bundle.
     * @param key the key under which this bundle is stored in the registry.
     * @param bundleName the name of the bundle. This function tries to create an instance of the ResourceBundle
     * @return an Option containing the created resource bundle
     */
    public static Option<ResourceBundle> addBundle ( final @NonNull String key, final @NonNull String bundleName )
    {
        return Try.of( () -> ResourceBundle.getBundle( bundleName ) )
                  .andThen( ( t ) -> addBundle( key, t ) )
                  .toOption();
                                                   
    }
    
    /**
     * Returns a formatted message identified by the key and bundle name.
     * @param key The key of the message
     * @return The formatted message if found, else the key
     */
    public static String getMessageFor( final @NonNull String key )
    {
        return getMessageFor( key, key );
        
    }
    
    /**
     * Returns the message for a key. If no message could be found for the key the default value is returned.
     * @param key The key of the message
     * @param defaultValue The value to be returned if no message could be found.
     * @return The message if found, else the default value
     */
    public static String getMessageFor( final @NonNull String key, final @NonNull String defaultValue )
    {
        String msg = defaultValue;
        
        String bundleKey = key.split( "\\." )[0];
        
        Option<ResourceBundle> resourceBundle = registry.get( bundleKey )
                                                        .orElse( createResourceBundleForKey( bundleKey ) )
                                                        .orElse( registry.get( DEFAULT_BUNDLE ) );
        
        if( resourceBundle.isDefined() )
        {   
            
            try
            {
                msg = resourceBundle.get().getString( key );
                                
            }
            catch( MissingResourceException exc )
            {
                // Resource not found = >nothing to do. Result is the default value
                msg = defaultValue;
                
            }
            
        }
        return msg;
        
    }
    
    /**
     * Tries to create a resource bundle for a key by determining the resource bundle to use via configuration.
     * If the resource bundle is found it is added to the list of known resource bundles.
     * 
     * @param key The key the resource bundle is requested for.
     * @return An Option containing the resource bundle if found
     */
    private static Option<ResourceBundle> createResourceBundleForKey( final @NonNull String key )
    {
        return Option.of( config.getOrDefault( "resources.bundle." + key, "" ) )
                     .flatMap( ( t  ) -> addBundle( key, t ) );
        
    }
    
    /**
     * Returns the message for a key. If no message could be found for the key the default value is returned.
     * @param key The key of the message
     * @param defaultValue The value to be returned if no message could be found.
     * @param values An array containing the value to fill the placeholder in the message.
     * @return The message if found, else the default value
     */
    public static String getMessageFor( final @NonNull String key, final @NonNull String defaultValue, final Object[] values )
    {
        return MessageFormat.format( getMessageFor( key, defaultValue ), values );

    }
    
    /**
     * Returns the message for a key. If no message could be found for the key the key is returned.
     * @param key The key of the message
     * @param values An array containing the value to fill the placeholder in the message.
     * @return The message if found, else the default value
     */
    public static String getMessageFor( final @NonNull String key, final Object[] values )
    {
        return getMessageFor( key, key, values );
        
    }

}
