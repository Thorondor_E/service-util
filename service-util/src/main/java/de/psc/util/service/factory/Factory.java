package de.psc.util.service.factory;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation which marks a class as a factory. An annotation processor creates a service file entry
 * for this class. The service file is for de.psc.util.service.factory.Factory
 * 
 */
@Retention( SOURCE )
@Target( TYPE )
public @interface Factory
{

}
