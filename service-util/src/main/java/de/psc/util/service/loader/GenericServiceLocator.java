package de.psc.util.service.loader;

import java.util.ServiceLoader;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;

/**
 * Utility class which loads implementations for a specific interface registered as service in the services file.
 *
 */
public final class GenericServiceLocator 
{
    /**
     * List with already created instances of singletons.
     */
    static private Map<Class<?>, Object > singletons = HashMap.empty();    

    private GenericServiceLocator() 
    {
        
    }

    /**
     * Loads an implementation of an interface. If more than one implementation exist the first found implementation is returned
     * which means that the result is not determined in that case. If the interface is annotated with SingletonService an already
     * created instance will be returned if it exists. If it does not exist an instance will be created and returned in subsequent calls.
     * 
     * @param <T>    Type of the class to be loaded
     * @param clazz  The interface class for which the implementation shall be returned.
     * @return An Option with the found implementation if exists, else Option.None
     */
    @SuppressWarnings( "unchecked" )
    public static <T> Option<T> locate( final Class<T> clazz ) 
    {
        return singletons.get( clazz )
                         .map(  ( t ) -> (T)t )
                         .orElse( loadFor( clazz ) ); 
                         
    }
    
    /**
     * Loads an implementation for a class. If more than one implementation exist the first found implementation is returned
     * which means that the result is not determined in that case. 
     * 
     * @param <T>   Type of the class to be loaded
     * @param clazz The interface class for which the implementation shall be returned.
     * @return An Option with the found implementation if exists, else Option.None
     */
    private static <T> Option<T> loadFor( final Class<T> clazz )
    {
        Option<T> service = locateAll( clazz ).peekOption();
        
        if( ( service.isDefined() )&&( clazz.isAnnotationPresent( SingletonService.class ) ) )
        {
            singletons = singletons.put( clazz, service.get() );
            
        }
        return service;
        
    }

    /**
     * Returns a list with the implementations of an interface.
     * 
     * @param <T>    Type of the class to be loaded
     * @param clazz The interface class for which the implementations shall be returned.
     * @return A list containing an instance per implementation.
     * 
     */
    public static <T> List<T> locateAll( final Class<T> clazz ) 
    {
        final ServiceLoader<T> iterator = ServiceLoader.load( clazz );
        return List.ofAll( iterator );

    }
    
}