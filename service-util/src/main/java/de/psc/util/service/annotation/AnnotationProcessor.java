package de.psc.util.service.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This annotation is used to mark a class as an annotation processor. The class must inherit from javax.annotation.processing.Processor.
 * This class is processed by the annotation processor AnnotationProcessorProcessor. This processor creates an entry in the file
 * javax.annotation.processing.Processor which is stored in the folder META-INF/services.
 *
 */
@Retention( SOURCE )
@Target( { TYPE, METHOD } )
public @interface AnnotationProcessor
{

}
