package de.psc.util.service.factory;

import java.lang.annotation.Annotation;

import de.psc.util.service.loader.GenericServiceLocator;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.NonNull;

/**
 * Abstract factory class which can be used for creating instances of classes depending on the key(s).
 * The classes to be handled by this factory must be marked by a custom annotation which defines the key(s)
 * a specific class implementation is assigned to. The type must be a String value. The specific implementation 
 * of a factory must implement the function which gets the annotation as parameter and returns the type
 * the implementation is assigned to. Additionally it is possible to use contexts. This means you can define 
 * different implementations for the same key. Which implementation will be used depends on the context which is set 
 * for the factory. If no context is defined for an implementation the default context is used. It is possible
 * to use the wildcard "?" as context. If an implementation is assigned to this context this implementation is returned
 * for any context if no context specific implementation exists.
 * 
 * Definition of the function: protected {@code Option<String>} getKey( V annotation );
 * or (if multiple keys are possible): protected  {@code List<String>} getKeys( V annotation );
 * The factory gets the implementations via the service loader.
 *
 * @param <T> The interface the implementations inherit from.
 * @param <V> The annotation which defines the type the implementation shall be assigned to.
 */
public abstract class AbstractServiceLoaderBasedFactory<T, V extends Annotation> implements GenericFactory<T>
{
    /**
     * The default context. If an implementation has no context defined this context is used.
     */
    public final static String DEFAULT_CONTEXT = "DEFAULT-CONTEXT";
    
    /**
     * Definition of the wildcard context
     */
    public final static String ALL_CONTEXTS = "?";
    
    /**
     * Maps with the assignments Key -> Implementation
     * The key is a combination of the context and the type of the class
     */
    private Map<String, List<Class<T>>> implementations = HashMap.empty();
    
    /**
     * The annotation class which defines the type
     */
    private Class<V> annotationClass;
    
    /**
     * The interface class the implementations must inherit from
     */
    private Class<T> interfaceClass;
    
    /**
     * The context to use
     */
    private String usedContext = DEFAULT_CONTEXT;
    
    
    /**
     * Constructor.
     * 
     * @param annotationClass The class of the annotation which is used to define the type of an implementation.
     * @param interfaceClass The class of the interface the implementations belong to
     * 
     * @throws NullPointerException in case the annotationClass or interfaceClass is null
     * 
     */
    public AbstractServiceLoaderBasedFactory( final @NonNull Class<V> annotationClass, final @NonNull Class<T> interfaceClass ) 
    {
        this.annotationClass = annotationClass;
        this.interfaceClass = interfaceClass;
        
        loadImplementations();
        
    }
    
    /**
     * Loads the implementations of an interface which shall be handled by this factory class.
     * The implementations must be annotated with the annotation class if they shall be recognized by this factory.
     * 
     */
    @SuppressWarnings( "unchecked" )
    private void loadImplementations()
    {
        // Get the registered implementations
        List<T> implList = GenericServiceLocator.locateAll( interfaceClass );
        
        for( T implementation : implList )
        {
            List<String> keys = getKeysForImplementation( implementation );
            
            for( String key : keys )
            {
                List<Class<T>> implForKey = implementations.get( key )
                                                           .getOrElse( List.empty() );
                implForKey = implForKey.append( (Class<T>)implementation.getClass() );
                implementations = implementations.put( key, implForKey );
                
            }
            
        }
        
    }
    
    /**
     * Determines the type of an implementation the implementation shall be assigned to.
     * 
     * @param implementation The implementation for which the type shall be determined.
     * @return An Option containing the type if found else Option.None
     * 
     */
    private List<String> getKeysForImplementation( final T implementation )
    {
        Option<String> context =  Option.of( implementation.getClass().getAnnotation( annotationClass ) )
                                        .flatMap(  ( t ) -> getContext( t ) )
                                        .orElse( Option.of(  DEFAULT_CONTEXT  ) );

        
        return Option.of( implementation.getClass().getAnnotation( annotationClass ) )
                     .map(  ( t ) -> getKeys( t ) )
                     .get()
                     .map(  ( t ) -> context.get() + "-" + t );
        
    }
    
    
    /**
     * Determines the type of an implementation. The factory assigns the implementation to this key.
     * 
     * @param annotation The annotation which defines the type of an implementation.
     * @return The type of implementation.
     */
    protected abstract List<String> getKeys( V annotation );
    
    /**
     * Determines the namespace of an implementation. The factory assigns the implementation to this namespace.
     * If no namespace is defined the default namespace is used.
     * 
     * @param annotation The annotation which defines the type of an implementation.
     * @return The type of implementation.
     */
    protected Option<String> getContext( V annotation )
    {
        return Option.of(  DEFAULT_CONTEXT );
        
    }
   
    
    
    /**
     * Returns the requested implementation for a key. If there exist more than one implementations for that key
     * the first one in the list will be returned.
     * This function first checks if there exists at least one implementation for which the key exactly matches the
     * combination of context and key. If there exists no such implementation the function searches for an implementation
     * with wildcard context.
     * 
     * @param key The type the implementation is requested for.
     * @return An Option containing an instance of the implementation if found, else Option.None
     * 
     */
    @Override
    public Option<T> getImplementationFor( final String key ) 
    {
        return implementations.get( usedContext + "-" + key )
                              .orElse(  implementations.get(  ALL_CONTEXTS + "-" + key ) )
                              .map( ( t ) -> t.head() )
                              .map(  ( t ) -> {
                                                  try
                                                  {
                                                      return t.newInstance();
                                                      
                                                  }
                                                  catch( InstantiationException | IllegalAccessException e )
                                                  {
                                                      return null;
                                    
                                                  }
                                
                                              } );     
        
    }
    
    /**
     * Returns all requested implementations for a key. This function returns the context specific implementations
     * as well as the implementations assigned to the wildcard context
     * 
     * @param key The type the implementation is requested for.
     * @return A List containing an instance per implementation if found.
     * 
     */
    @Override
    public List<T> getImplementationsFor( final String key ) 
    {
        return implementations.get( usedContext + "-" + key )
                              .getOrElse( List.empty() )
                              .appendAll( implementations.get( ALL_CONTEXTS + "-" + key )
                                                         .getOrElse( List.empty() ) )
                              .map(  ( t ) -> {
                                                  try
                                                  {
                                                      return t.newInstance();
                                                      
                                                  }
                                                  catch( InstantiationException | IllegalAccessException e )
                                                  {
                                                      return null;
                                    
                                                  }
                                
                                              } );     
        
    }
    
    
    /* (nicht-Javadoc)
     * @see de.psc.util.service.factory.GenericFactory#getInterfaceClass()
     */
    @Override
    public Class<T> getInterfaceClass()
    {
        return interfaceClass;
        
    }
    
    /* (nicht-Javadoc)
     * @see de.psc.util.service.factory.GenericFactory#setContext(java.lang.String)
     */
    @Override
    public void setContext( final @NonNull String context )
    {
        usedContext = context;
        
    }
    /* (nicht-Javadoc)
     * @see de.psc.util.service.factory.GenericFactory#setDefaultContext()
     */
    @Override
    public void setDefaultContext()
    {
        usedContext = DEFAULT_CONTEXT;
        
    }

}
