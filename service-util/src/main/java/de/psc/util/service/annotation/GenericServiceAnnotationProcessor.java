package de.psc.util.service.annotation;

import static com.google.auto.common.AnnotationMirrors.getAnnotationValue;
import static com.google.auto.common.MoreElements.getAnnotationMirror;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleAnnotationValueVisitor8;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import com.google.auto.common.MoreTypes;
import com.google.common.collect.ImmutableSet;

import io.vavr.collection.HashMultimap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Multimap;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;
import io.vavr.collection.TreeSet;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.NonNull;

/**
 * Generic annotation processor which creates entries in the service file (like
 * Googles auto-service) For each annotation which shall be handled by this
 * generic processor a subclass is needed which defines the concrete annotation
 * to be handled. The annotation to be handled must have a property called
 * "service" of type {@code Class<?>} which defines the interface the implementation is
 * used for. As an alternative the inheriting class can override the function getServiceFieldOfClass
 * of this class. The definition of this function is
 * {@code Option<DeclaredType>} getServiceFieldOfClass( final AnnotationMirror annotationMirror )
 * 
 */
@SupportedOptions( { "debug", "verify" } )
public abstract class GenericServiceAnnotationProcessor<T extends Annotation> extends AbstractProcessor
{

    static final String MISSING_SERVICES_ERROR = "No service interfaces provided for element!";

    /**
     * Maps the class names of service provider interfaces to the class names of the
     * concrete classes which implement them.
     * 
     */
    private Multimap<String, String> providers = HashMultimap.withSeq().empty();

    /**
     * The annotation class handled by this processor
     */
    private Class<T> annotationClass;

    /**
     * Constructor which takes an example of the annotation class this processor is
     * responsible for.
     * 
     * @param annotationClass The class this annotation processor is responsible for
     */
    public GenericServiceAnnotationProcessor( final @NonNull Class<T> annotationClass )
    {
        this.annotationClass = annotationClass;

    }

    /* (nicht-Javadoc)
     * @see javax.annotation.processing.AbstractProcessor#getSupportedAnnotationTypes()
     */
    @Override
    public ImmutableSet<String> getSupportedAnnotationTypes()
    {
        return ImmutableSet.of( annotationClass.getName() );

    }

    /* (nicht-Javadoc)
     * @see javax.annotation.processing.AbstractProcessor#getSupportedSourceVersion()
     */
    @Override
    public SourceVersion getSupportedSourceVersion()
    {
        return SourceVersion.latestSupported();
        
    }

    /* (nicht-Javadoc)
     * @see javax.annotation.processing.AbstractProcessor#process(java.util.Set, javax.annotation.processing.RoundEnvironment)
     */
    @Override
    public boolean process( final java.util.Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv )
    {

        try
        {
            return processImpl( List.ofAll( annotations.stream() ), roundEnv );

        }
        catch( Exception e )
        {
            // We don't allow exceptions of any kind to propagate to the compiler
            StringWriter writer = new StringWriter();
            e.printStackTrace( new PrintWriter( writer ) );
            fatalError( writer.toString() );
            return true;
        }
    }

    /**
     * Processes the annotation. In case the is the last processing round the service file is created.
     * Otherwise the information about the service provider classes is collected.
     * 
     * @param annotations  the annotation types requested to be processed
     * @param roundEnv environment for information about the current and prior round
     * @return always true
     */
    private boolean processImpl( final List<? extends TypeElement> annotations, final RoundEnvironment roundEnv )
    {
        
        if( roundEnv.processingOver() )
        {
            generateConfigFiles();
            
        }
        else
        {
            processAnnotations( annotations, roundEnv );
            
        }
        return true;
        
    }

    /**
     * Processes the annotation which means collects the information about the service implementation classes.
     * 
     * @param annotations the annotation types requested to be processed
     * @param roundEnv environment for information about the current and prior round
     */
    private void processAnnotations( final List<? extends TypeElement> annotations, final RoundEnvironment roundEnv )
    {
        List<? extends Element> elements = List.ofAll( roundEnv.getElementsAnnotatedWith( annotationClass ).stream() );

        log( annotations.toString() );
        log( elements.toString() );

        for( Element e : elements )
        {
            TypeElement providerImplementer = (TypeElement)e;
            AnnotationMirror annotationMirror = getAnnotationMirror( e, annotationClass ).get();
            
            Option<DeclaredType> providerInterface = getServiceFieldOfClass( annotationMirror );
            
            if( providerInterface.isEmpty() )
            {
                error( MISSING_SERVICES_ERROR, e, annotationMirror );
                continue;
                
            }
            
            TypeElement providerType = MoreTypes.asTypeElement( providerInterface.get() );
            
            log( "provider interface: " + providerType.getQualifiedName() );
            log( "provider implementer: " + providerImplementer.getQualifiedName() );

            if( checkImplementer( providerImplementer, providerType ) )
            {
                providers = providers.put( getBinaryName( providerType ), getBinaryName( providerImplementer ) );

            }
            else
            {
                String message = "ServiceProviders must implement their service provider interface. " + providerImplementer.getQualifiedName()
                                + " does not implement " + providerType.getQualifiedName();
                error( message, e, annotationMirror );

            }                
            
        }
    
    }

    private void generateConfigFiles()
    {
        Filer filer = processingEnv.getFiler();

        for( String providerInterface : providers.keySet() )
        {
            String resourceFile = "META-INF/services/" + providerInterface;
            log( "Working on resource file: " + resourceFile );
            
            try
            {
                SortedSet<String> allServices = TreeSet.empty();
                
                try
                {
                    // would like to be able to print the full path
                    // before we attempt to get the resource in case the behavior
                    // of filer.getResource does change to match the spec, but there's
                    // no good way to resolve CLASS_OUTPUT without first getting a resource.
                    FileObject existingFile = filer.getResource( StandardLocation.CLASS_OUTPUT, "", resourceFile );
                    log( "Looking for existing resource file at " + existingFile.toUri() );
                    Set<String> oldServices = ServicesFiles.readServiceFile( existingFile.openInputStream() );
                    log( "Existing service entries: " + oldServices );
                    allServices = allServices.addAll( oldServices );
                    
                    
                }
                catch( IOException e )
                {
                    // According to the javadoc, Filer.getResource throws an exception
                    // if the file doesn't already exist. In practice this doesn't
                    // appear to be the case. Filer.getResource will happily return a
                    // FileObject that refers to a non-existent file but will throw
                    // IOException if you try to open an input stream for it.
                    log( "Resource file did not already exist." );
                    
                }

                Set<String> newServices = HashSet.ofAll( providers.get( providerInterface )
                                                                  .getOrElse( () -> HashSet.empty() ) );
                
                if( allServices.containsAll( newServices ) )
                {
                    log( "No new service entries being added." );
                    return;
                    
                }

                allServices = allServices.addAll( newServices );
                log( "New service file contents: " + allServices );
                FileObject fileObject = filer.createResource( StandardLocation.CLASS_OUTPUT, "", resourceFile );
                OutputStream out = fileObject.openOutputStream();
                ServicesFiles.writeServiceFile( allServices, out );
                out.close();
                log( "Wrote to: " + fileObject.toUri() );
                
            }
            catch( IOException e )
            {
                fatalError( "Unable to create " + resourceFile + ", " + e );
                return;
                
            }
            
        }
        
    }

    /**
     * Verifies {@link ServiceProvider} constraints on the concrete provider class.
     * Note that these constraints are enforced at runtime via the ServiceLoader,
     * we're just checking them at compile time to be extra nice to our users.
     */
    private boolean checkImplementer( final TypeElement providerImplementer, final TypeElement providerType )
    {
        String verify = processingEnv.getOptions().get( "verify" );
        
        if( verify == null || !Boolean.valueOf( verify ) )
        {
            return true;
            
        }
        Types types = processingEnv.getTypeUtils();
        return types.isSubtype( providerImplementer.asType(), providerType.asType() );
        
    }

    /**
     * Returns the binary name of a reference type. For example,
     * {@code com.google.Foo$Bar}, instead of {@code com.google.Foo.Bar}.
     * @element The element for which the name is requested
     *
     */
    private String getBinaryName( final TypeElement element )
    {
        return getBinaryNameImpl( element, element.getSimpleName().toString() );
        
    }

    /**
     * Returns the binary name of a reference type. For example,
     * {@code com.google.Foo$Bar}, instead of {@code com.google.Foo.Bar}.
     * @element The element for which the name is requested
     * @className The name of the class
     * @return The binary name of the element
     */
    private String getBinaryNameImpl( final TypeElement element, final String className )
    {
        Element enclosingElement = element.getEnclosingElement();

        if( enclosingElement instanceof PackageElement )
        {
            PackageElement pkg = (PackageElement)enclosingElement;
            
            if( pkg.isUnnamed() )
            {
                return className;
                
            }
            return pkg.getQualifiedName() + "." + className;
            
        }
        TypeElement typeElement = (TypeElement)enclosingElement;
        return getBinaryNameImpl( typeElement, typeElement.getSimpleName() + "$" + className );
        
    }

    /**
     * Returns the contents of a {@code Class[]}-typed "service" field in a given
     * {@code annotationMirror}. 
     * This function can be overriden by subclasses in order to use an own implementation for determination.
     * @param annotationMirror The annotation mirror instance which is used to determine the service.
     * @return An Option containing a Declared type which represents the service.
     */
    protected Option<DeclaredType> getServiceFieldOfClass( final AnnotationMirror annotationMirror )
    {
        return getAnnotationValue( annotationMirror, "service" ).accept( new SimpleAnnotationValueVisitor8<Option<DeclaredType>, Void>() 
        {
            @Override
            public Option<DeclaredType> visitType( TypeMirror typeMirror, Void v )
            {
                return Try.ofSupplier( () -> { return MoreTypes.asDeclared( typeMirror ); } )
                          .map( ( t ) -> Option.of( t ) )
                          .getOrElseGet( ( t ) -> Option.none() );
                
            }

        }, null );
        
    }

    private void log( final String msg )
    {
        
        if( processingEnv.getOptions().containsKey( "debug" ) )
        {
            processingEnv.getMessager().printMessage( Kind.NOTE, msg );
            
        }
        
    }

    private void error( final String msg, final Element element, final AnnotationMirror annotation )
    {
        processingEnv.getMessager().printMessage( Kind.ERROR, msg, element, annotation );
        
    }

    private void fatalError( final String msg )
    {
        processingEnv.getMessager().printMessage( Kind.ERROR, "FATAL ERROR: " + msg );
        
    }
    
}
