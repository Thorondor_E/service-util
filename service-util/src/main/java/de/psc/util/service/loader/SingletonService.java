package de.psc.util.service.loader;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
 
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
 
/**
 * This annotation marks interface which allow only one instance
 * 
 */
@Retention( RUNTIME )
@Target( TYPE )
public @interface SingletonService
{
}
 