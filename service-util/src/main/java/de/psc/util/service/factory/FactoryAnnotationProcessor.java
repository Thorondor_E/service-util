package de.psc.util.service.factory;

import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import com.google.auto.service.AutoService;

import de.psc.util.service.annotation.GenericServiceAnnotationProcessor;
import io.vavr.control.Option;

/**
 * This class is responsible for managing the annotation AnnotationProcessor. It generates a service entry file for
 * subclasses of the Processor class.
 * 
 */
@SupportedAnnotationTypes( "de.psc.util.service.factory.Factory" )
@AutoService( Processor.class )
public class FactoryAnnotationProcessor extends GenericServiceAnnotationProcessor<Factory>
{
    
    public FactoryAnnotationProcessor()
    {
        super( Factory.class );
        
    }
    
    /**
     * Returns the contents of a {@code Class[]}-typed "service" field in a given
     * {@code annotationMirror}.
     *  
     */
    @Override
    protected Option<DeclaredType> getServiceFieldOfClass( final AnnotationMirror annotationMirror )
    {
        Elements elements = processingEnv.getElementUtils();
        Types types = processingEnv.getTypeUtils();

        // These are elements, they never have generic types associated
        TypeElement processorElement = elements.getTypeElement( GenericFactory.class.getName());

        // Build a Type: List<String>
        DeclaredType processorType = types.getDeclaredType( processorElement );
        return Option.of(  processorType );

    }
   
    

}
