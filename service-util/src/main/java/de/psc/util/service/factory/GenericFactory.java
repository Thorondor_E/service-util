package de.psc.util.service.factory;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.NonNull;

/**
 * Interface which is the base for all generic factories.
 * The factory stores the information which implementation belongs to a key. It is possible that there exist
 * more than one implementation for a key.
 *
 * @param <T> The type of the classes which are handled by this factory.
 * 
 */
public interface GenericFactory<T>
{
    /**
     * Returns the requested implementation. If there exist more than one implementations for the same key the first found 
     * implementation is returned.
     * 
     * @param key The type the implementation is requested for.
     * @return An Option containing an instance of the implementation if found, else Option.None
     * 
     */
   Option<T> getImplementationFor( final String key ) ;
   
   /**
    * Returns all implementations for the key.
    * 
    * @param key The type the implementation is requested for.
    * @return A list containing all instances of the implementation if found, else an empty list.
    */
   List<T> getImplementationsFor( final String key );
   
   /**
    * Returns the interface class this factory returns implementations of
    * 
    * @return The interface class
    */
   Class<T> getInterfaceClass();
   
   /**
    * The name of the namespace to use. Only elements assigned to that namespace will be used for searching the matching implementation.
    * 
    * @param context The namespace to be used by the factory
    */
   public void setContext( final @NonNull String context );
   
   /**
    * Sets the context back to the default context 
    */
   public void setDefaultContext();
   
    

}
