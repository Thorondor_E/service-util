package de.psc.util.service.factory;

import de.psc.util.service.loader.GenericServiceLocator;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.NonNull;

/**
 * Simple Utility class which returns a factory for an interface class.
 * The factories handled by this class are determined by the service loader mechanisms
 * and must be defined in the file META-INF/services/de.psc.util.service.factory.GenericFactory
 * 
 */
public class GenericFactoryFactory
{
    private static Map< String, GenericFactory<?>> factories = HashMap.empty();
    
    
    /**
     * Returns an Option containing an instance of a factory if exists. Else an empty Option.
     * 
     * @param interfaceClass The interface the factory is requested for.
     * @return An Option containing the factory if found. Else Option.None.
     * 
     */
    public static Option<GenericFactory<?>> getFactoryFor( @NonNull final Class<?> interfaceClass )
    {   
        Option<GenericFactory<?>> factory = factories.get( interfaceClass.getCanonicalName() );
        
        if( factory.isEmpty() )
        {
            factory =  GenericServiceLocator.locateAll( GenericFactory.class )
                                            .find(  ( t ) -> interfaceClass.getCanonicalName().equals( t.getInterfaceClass().getCanonicalName() ) )
                                            .map(  ( t ) -> (GenericFactory<?>)t );
            
            if( factory.isDefined() )
            {
                factories = factories.put( interfaceClass.getCanonicalName(), factory.get() );
                
            }
        
        }  
        return factory;

    }
    
}

