package de.psc.util.service.annotation;

import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import com.google.auto.service.AutoService;

import io.vavr.control.Option;

/**
 * This class is responsible for managing the annotation AnnotationProcessor. It generates a service entry file for
 * subclasses of the Processor class.
 * 
 */
@SupportedAnnotationTypes( "de.psc.util.service.annotation.AnnotationProcessor" )
@AutoService( Processor.class )
public class AnnotationProcessorProcessor extends GenericServiceAnnotationProcessor<AnnotationProcessor>
{
    
    public AnnotationProcessorProcessor()
    {
        super( AnnotationProcessor.class );
        
    }
    
    /**
     * Returns the contents of a {@code Class[]}-typed "service" field in a given
     * {@code annotationMirror}. 
     * This function can be overriden by subclasses in order to use an own implementation for determination.
     */
    @Override
    protected Option<DeclaredType> getServiceFieldOfClass( final AnnotationMirror annotationMirror )
    {
        
        Elements elements = processingEnv.getElementUtils();
        Types types = processingEnv.getTypeUtils();

        // These are elements, they never have generic types associated
        TypeElement processorElement = elements.getTypeElement( Processor.class.getName());

        // Build a Type: List<String>
        DeclaredType processorType = types.getDeclaredType( processorElement );
        
        return Option.of(  processorType );

    }
   
    

}
