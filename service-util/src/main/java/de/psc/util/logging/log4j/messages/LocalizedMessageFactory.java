package de.psc.util.logging.log4j.messages;

import org.apache.logging.log4j.message.AbstractMessageFactory;
import org.apache.logging.log4j.message.Message;

/**
 * Factory which creates an instance of a LocalizedMessage
 *
 */
@SuppressWarnings( "serial" )
public class LocalizedMessageFactory extends AbstractMessageFactory
{
    /* (non-Javadoc)
     * @see org.apache.logging.log4j.message.AbstractMessageFactory#newMessage(java.lang.String)
     */
    @Override
    public Message newMessage( final String messageKey ) 
    {
        return new LocalizedMessage( messageKey );
        
    }

    /* (non-Javadoc)
     * @see org.apache.logging.log4j.message.MessageFactory#newMessage(java.lang.String, java.lang.Object[])
     */
    @Override
    public Message newMessage( final String messageKey, final Object... params )
    {
        return new LocalizedMessage( messageKey, params );
        
    }
}
