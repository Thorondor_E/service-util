    package de.psc.util.logging.log4j.messages;

import org.apache.logging.log4j.message.ParameterizedMessage;

import de.psc.util.internationalization.Messages;

/**
 * Message class which is able to resolve message vie resource bundle. The resource bundle to use must be 
 * defined in the file META-INF/javaconfiguration.properties (see Messages). If no such bundle exists or if 
 * no message can be found for the key the text is logged. 
 * This class inherits from ParamterizedMessage which means placeholder can be used and filled with 
 * paramters. Placeholders are marked by {} in the text. 
 * 
 * @author peter
 *
 */
@SuppressWarnings( "serial" )
public class LocalizedMessage extends ParameterizedMessage
{
    
    /**
     * Creates a localized message based in the key.
     * 
     * @param      messageKey The key of the message or the message itself. 
     *                        The message "format" string. This will be a String
     *                        containing "{}" placeholders where parameters
     *                        should be substituted.
     * @param      arguments  The arguments for substitution.
     * @param      throwable  A Throwable.
     * @deprecated            Use constructor LocalizedMessage(String,
     *                        Object[], Throwable) instead
     */
    @Deprecated
    public LocalizedMessage( final String messageKey, final String[ ] arguments, final Throwable throwable )
    {
        super( Messages.getMessageFor( messageKey ), arguments, throwable );
        
    }

    /**
     * Creates a localized message.
     * 
     * @param messageKey The key of the message or the message itself. 
     *                   The message "format" string. This will be a String
     *                   containing "{}" placeholders where parameters
     *                   should be substituted.
     * @param arguments  The arguments for substitution.
     * @param throwable  A Throwable.
     */
    public LocalizedMessage( final String messageKey, final Object[ ] arguments, final Throwable throwable )
    {
        super( Messages.getMessageFor(  messageKey ), arguments, throwable );
        
    }

    /**
     * Constructs a LocalizeddMessage which contains the arguments converted to
     * String as well as an optional Throwable.
     *
     * <p>
     * If the last argument is a Throwable and is NOT used up by a placeholder in
     * the message pattern it is returned in {@link #getThrowable()} and won't be
     * contained in the created String[]. If it is used up {@link #getThrowable()}
     * will return null even if the last argument was a Throwable!
     * </p>
     *
     * @param messageKey The key of the message or the message itself. 
     *                   The message "format" string. This will be a String
     *                   containing "{}" placeholders where parameters
     *                   should be substituted..
     * @param arguments  the argument array to be converted.
     */
    public LocalizedMessage( final String messageKey, final Object... arguments )
    {
        super( Messages.getMessageFor( messageKey ), arguments );
        
    }

    /**
     * Constructor with a pattern and a single parameter.
     * 
     * @param messageKey The key of the message or the message itself. 
     *                   The message "format" string. This will be a String
     *                   containing "{}" placeholders where parameters
     *                   should be substituted.
     * @param arg        The parameter.
     */
    public LocalizedMessage( final String messageKey, final Object arg )
    {
        super( Messages.getMessageFor( messageKey ), arg );
        
    }

    /**
     * Constructor with a pattern and two parameters.
     * 
     * @param messageKey The key of the message or the message itself. 
     *                   The message "format" string. This will be a String
     *                   containing "{}" placeholders where parameters
     *                   should be substituted.
     * @param arg0       The first parameter.
     * @param arg1       The second parameter.
     */
    public LocalizedMessage( final String messageKey, final Object arg0, final Object arg1 )
    {
        super( Messages.getMessageFor( messageKey ), new Object[ ] { arg0, arg1 } );
        
    }
    
}
